<?php

require_once '../Objects/Database.php';
require_once '../Objects/product.php';

if (!empty($_POST['SKU'])) {

    $database = new Database();
    $db = $database->getConnection();

    $prodObj = new Product($db);

    $sku = $_POST['SKU'];

    $array = $prodObj->checkSKU($sku);
	
	$valid = empty($array) ? 'true' : 'false';
    
    echo $valid;
}
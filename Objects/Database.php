<?php

class Database{

    private $host = "localhost";
    private $db_name = "id16297434_product_db";
    private $username = "id16297434_admin";
    private $password = "I8xl~%1rCf&%iNO7";
    private $port = 3306;
    public $conn;

    // get the database connection
    public function getConnection()
    {
        $this->conn = null;

        try{
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name . ";port=" . $this->port, $this->username, $this->password);
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }

        return $this->conn;
    }
}
?>
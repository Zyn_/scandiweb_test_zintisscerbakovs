<?php

class Product
{
    protected $conn;
    protected $tab_name = "Products";

    public $sku;
    public $name;
    public $price;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function readProducts()
    {
        $query = "SELECT * from " . $this->tab_name . " ORDER BY id";

        $stmt = $this->conn->prepare($query);

        if ($stmt->execute()) {
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    function delete($id)
    {
        $query = "DELETE FROM " . $this->tab_name . " WHERE id = :id";

        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':id', $id);

        return $stmt->execute();

    }

    function checkSKU($sku)
    {
        $query = "SELECT * FROM " . $this->tab_name . " WHERE SKU = :sku";

        $stmt = $this->conn->prepare($query);

        $sku = htmlspecialchars(strip_tags($sku));

        $stmt->bindParam(":sku", $sku);

        if ($stmt->execute()) {
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
    }

}

interface productInterface {
    public function addProduct($sku , $name , $price );
    public function getProductsByType();
}

class Disc extends Product implements productInterface
{
    public $size;

    public function addProduct($sku , $name , $price )
    {
        //write query
        $query = "INSERT INTO
                    " . $this->tab_name . "
                SET
                    SKU=:sku, Name=:name, Price=:price,Size=:size";

        $stmt = $this->conn->prepare($query);

        // posted values
        $this->sku   = htmlspecialchars(strip_tags($sku));
        $this->name  = htmlspecialchars(strip_tags($name));
        $this->price = htmlspecialchars(strip_tags($price));
        $this->size  = htmlspecialchars(strip_tags($_POST['size']));

        // bind values
        $stmt->bindParam(":sku", $this->sku);
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":price", $this->price);
        $stmt->bindParam(":size", $this->size);

        return $stmt->execute();
    }

    public function getProductsByType()
    {
        $query = "SELECT id,SKU,Name,Price,Size as Special 
				  FROM " . $this->tab_name .
				  " WHERE Size IS NOT NULL";

        $stmt = $this->conn->prepare($query);

        if ($stmt->execute()) {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            for($i=0;$i<sizeof($result);$i++){
                $result[$i]['Special'] = "Size: ". $result[$i]['Special'] ." MB";
            }
            return $result;
        }
    }
}

class Furniture extends Product implements productInterface
{
    public $dimensions;

    public function addProduct($sku , $name , $price )
    {

        $query = "INSERT INTO
                    " . $this->tab_name . "
                SET
                    SKU=:sku, Name=:name, Price=:price,Dimensions=:dimensions";

        $stmt = $this->conn->prepare($query);

        $special = strval($_POST['height']) . "x" .
            strval($_POST['width']) . "x" .
            strval($_POST['length']);

        $this->sku        = htmlspecialchars(strip_tags($sku));
        $this->name       = htmlspecialchars(strip_tags($name));
        $this->price      = htmlspecialchars(strip_tags($price));
        $this->dimensions = htmlspecialchars(strip_tags($special));

        $stmt->bindParam(":sku", $this->sku);
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":price", $this->price);
        $stmt->bindParam(":dimensions", $this->dimensions);

        return $stmt->execute();
    }

    public function getProductsByType()
    {
        $query = "SELECT id,SKU,Name,Price,Dimensions as Special 
				 FROM " . $this->tab_name .
				 " WHERE Dimensions IS NOT NULL";

        $stmt = $this->conn->prepare($query);

        if ($stmt->execute()) {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            for($i=0;$i<sizeof($result);$i++){
                $result[$i]['Special'] = "Dimensions: ". $result[$i]['Special'];
            }
            return $result;
        }
    }
}

class Book extends Product implements productInterface
{
    public $weight;

    public function addProduct($sku , $name , $price)
    {
        $query = "INSERT INTO
                    " . $this->tab_name . "
                SET
                    SKU=:sku, Name=:name, Price=:price,Weight=:weight";

        $stmt = $this->conn->prepare($query);

        $this->sku    = htmlspecialchars(strip_tags($sku));
        $this->name   = htmlspecialchars(strip_tags($name));
        $this->price  = htmlspecialchars(strip_tags($price));
        $this->weight = htmlspecialchars(strip_tags($_POST['weight']));

        $stmt->bindParam(":sku", $this->sku);
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":price", $this->price);
        $stmt->bindParam(":weight", $this->weight);

        return $stmt->execute();
    }


    public function getProductsByType()
    {
        $query = "SELECT id,SKU,Name,Price,Weight as Special 
				 FROM " . $this->tab_name . 
				 " WHERE Weight IS NOT NULL";

        $stmt = $this->conn->prepare($query);

        if ($stmt->execute()) {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            for($i=0;$i<sizeof($result);$i++){
                $result[$i]['Special'] = "Weight: ". $result[$i]['Special'] ." KG";
            }
            return $result;
        }
    }
}
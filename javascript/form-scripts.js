
function prodType(prod){
    const discAtt = document.getElementById("Disc_att");
    const bookAtt = document.getElementById("book_att");
    const furnAtt = document.getElementById("furn_att");

    discAtt.style.display="none";
    bookAtt.style.display="none";
    furnAtt.style.display="none";

    switch(prod){
        case "Disc":
            discAtt.style.display="block";
            break;
        case "Book":
            bookAtt.style.display="block";
            break;
        case "Furniture":
            furnAtt.style.display="block";
            break;
    }

}

$(document).ready(function(){
    $("form[name='addForm']").validate({
        errorClass: 'error',
        ignore: ":hidden",
        rules:{
            SKU:{
                required:true,
                remote: {
                    url: "../validation/check-sku.php",
                    type: "post",
                }
            },
            Name: {
                required:true,
            },
            Price:{
                required:true,
                number: true,
            },
            selection:{
                required:true,
            },
            size:{
                required:true,
                number: true,
            },
            weight:{
                required:true,
                number: true,
            },
            height:{
                required:true,
                number: true,
            },
            length:{
                required:true,
                number: true,
            },
            width:{
                required:true,
                number: true,
            },
        },
        messages:{
            SKU:{                
                remote: "Product with such SKU already exists",
            },
            selection:{
                required:"Choose the category of product",
            },
            submitHandler: function() {
                $("#addForm").submit();
            }
        }

    })
});


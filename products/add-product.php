<?php

require_once "addScript.php";

?>
<!DOCTYPE html>
<html>

<head>
    <title>Add new Product</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css">

</head>
<style>

</style>

<body>
<div class="container">

    <div class="PrAd">
        <h2>
            <b class="title">Add a product</b>
            <a type="button" href="../index.php" class="btn btn-secondary btn-test" style="float:right;" id="cancel-button">Cancel</a>
            <input type="submit" name="submitBtn" class="btn btn-secondary btn-test" style="float:right;" value="Submit" form="addForm">

        </h2>
        <hr>
    </div>

    <form method="POST" action="add-product.php" id="addForm" name="addForm">
        <div class="form-group row align-items-center">
            <label for="SKU" class="col-sm-1 col-form-label teksts">SKU:</label>
            <div class="col-sm-10">
                <input type="text" id="SKU" name="SKU">
            </div>
        </div>

        <div class="form-group row">
            <label for="Name" class="col-sm-1 col-form-label teksts">Name:</label>
            <div class="col-sm-10">
                <input type="text" id="Name" name="Name"><span id="error-name"></span>
            </div>
        </div>

        <div class="form-group row">
            <label for="Price" class="col-sm-1 col-form-label teksts">Price($):</label>
            <div class="col-sm-10">
                <input type="number" step="0.01" id="Price" name="Price"><span id="error-price"></span>
            </div>
        </div>

        <div class="form-group row">
            <label for="Category" class="col-sm-1 col-form-label teksts">Category:</label>
            <div class="col-sm-2">
                <select class="custom-select mr-sm-2" id="type" name="selection" onChange="prodType(this.value);">
                    <option value="" selected>Choose...</option>
                    <option value="Disc">DVD</option>
                    <option value="Book">Book</option>
                    <option value="Furniture">Furniture</option>
                </select>
            </div>
        </div>

        <div class="fieldbox" id="Disc_att">
            <div class="form-group row">
                <label for="Size" class="col-sm-1 col-form-label teksts">Size(MB):</label>
                <div class="col-sm-10">
                    <input type="number" name="size" value="" id="size">
                </div>
                <div class="col-sm-10">
                    <p class="text-left teksts">Please provide DVD size in megabytes(MB).</p>
                </div>
            </div>
        </div>

        <div class="fieldbox" id="book_att">
            <div class="form-group row">
                <label for="Weight" class="col-sm-1 col-form-label teksts">Weight(KG):</label>
                <div class="col-sm-10">
                    <input type="number" step="0.01" name="weight" value="" id="weight">
                </div>
                <div class="col-sm-10">
                    <p class="text-left teksts">Please provide weight in kilograms.</p>
                </div>
            </div>
        </div>
        <div class="fieldbox" id="furn_att">
            <div class="form-group row">
                <label for="Price" class="col-sm-1 col-form-label teksts">Height(CM):</label>
                <div class="col-sm-10">
                    <input type="number" step="0.01" name="height" value="" id="height">
                </div>
            </div>
            <div class="form-group row">
                <label for="Price" class="col-sm-1 col-form-label teksts">Width(CM):</label>
                <div class="col-sm-10">
                    <input type="number" step="0.01" name="width" value="" id="width">
                </div>
            </div>
            <div class="form-group row">
                <label for="Price" class="col-sm-1 col-form-label teksts">Length(CM):</label>
                <div class="col-sm-10">
                    <input type="number" step="0.01" name="length" value="" id="length">
                </div>
                <div class="col-sm-10">
                    <p class="text-left teksts">Please provide dimensions in HxWxL format.</p>
                </div>
            </div>
        </div>
    </form>
    <div class="footer">
        <hr>
        <p>Scandiweb Test assignment</p>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../javascript/form-scripts.js">
</script>
</body>

</html>

<?php

//ini_set('display_errors', '1');
//ini_set('display_startup_errors', '1');
//error_reporting(E_ALL);

require_once '../Objects/Database.php';
require_once '../Objects/product.php';

$database = new Database();
$db = $database->getConnection();

if(isset($_POST["submitBtn"])) {
    $objType = $_POST['selection'];
    //$_POST product attribute in the object class method
    if ((new $objType($db))->addProduct(strtoupper($_POST['SKU']),$_POST['Name'],$_POST['Price'])) {
        header("Location:../index.php");
        exit;
    } else {
        echo "Error in adding a product";
    }
}
?>

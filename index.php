<?php

//ini_set('display_errors', '1');
//ini_set('display_startup_errors', '1');
//error_reporting(E_ALL);

require_once 'Objects/Database.php';
require_once 'Objects/product.php';

$database = new Database();
$db = $database->getConnection();

$types=array("Disc","Book","Furniture");

if(isset($_POST['deleteBtn']) && !empty($_POST['checkbox'])){
    $prodObj = new Product(db: $db);
    $checkarray = $_POST['checkbox'];
    foreach ($checkarray as $id) {
        $prodObj->delete($id);
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>List of Products</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css" />

</head>

<body>
<div class="container">

    <div class="PrList">
        <h2>
            <b class="title">Product List</b>
            <a type="button" href="products/add-product.php" class="btn btn-secondary btn-test" style="padding:5px;float: right" role="button" >Add</a>
            <input type="submit" name="deleteBtn" id="deleteBtn" class="btn btn-secondary btn-test" style="float:right;" value="Delete" form="listForm" >
        </h2>
        <hr>
    </div>

    <form method="post" action="index.php" id="listForm" name="listForm">
    <div class="container-fluid mt-4">
        <div class="row justify-content-right">
            <?php
            //prints products based on called object method order
            foreach($types as $objType){
                $prodArray = (new $objType($db))->getProductsByType();
                foreach ($prodArray as $prod) {
                    ?>
                    <div class="col-auto mb-3">
                        <div class="card text-center" style="width: 15rem;">
                            <div class="card-body">
                                <div class="form-check">
                                    <input class="form-check-input" name="checkbox[]" type="checkbox" value="<?php echo $prod['id']; ?>" id="checkDelete">
                                    <label class="form-check-label" for="checkDelete">
                                    </label>
                                </div>
                                <p class="card-text"><?php echo $prod['SKU'] ?></p>
                                <p class="card-text"><?php echo $prod['Name'] ?></p>
                                <p class="card-text"><?php echo $prod['Price']." $" ?></p>
                                <p class="card-text"><?php echo $prod['Special'] ?></p>
                            </div>
                        </div>
                    </div>
                <?php } }?>

        </div>
    </div>
    </form>
    <br>
    <div class="footer">
        <hr>
        <p>Scandiweb Test assignment</p>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>